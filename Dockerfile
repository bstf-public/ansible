FROM python:3.9.6-slim-buster

RUN apt update && apt install -y openssh-client wget curl \
    && pip --no-cache-dir install ansible==2.9.24 \
    && ln -s /usr/bin/python3.9 /usr/bin/python

RUN wget -O helm.tar.gz https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz \
    && tar -zxvf helm.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm3

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.24.1/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl
